from flask import Flask, render_template, request
from pandas import DataFrame
import numpy as np
import model as md
import requests
import pickle
import json
import os
  
app = Flask(__name__)
  
@app.route('/forecast', methods = ['GET'])
def postJsonHandler():
    data=md.forecast_result()
    return data

if __name__ == '__main__':
    app.run(debug=True)