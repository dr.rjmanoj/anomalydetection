import boto3
import base64
import json
import pandas as pd
import numpy as np
import snowflake.connector as sf
import statsmodels.api as sm
import statsmodels.tsa.api as smt
from sklearn.metrics import mean_squared_error
from sqlalchemy import create_engine
from snowflake.sqlalchemy import URL
from botocore.exceptions import ClientError

def read_data():
    session = boto3.session.Session()
    client = session.client(service_name='secretsmanager', region_name="us-east-2",
             aws_access_key_id='AKIAUJ5FKMH75SUPJWFL',aws_secret_access_key='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')
    try:
        get_secret_value_response = client.get_secret_value(SecretId="sagemaker_snowflake")        
    except ClientError as e:
        if e.response['Error']['Code'] == 'DecryptionFailureException':
            raise e
        elif e.response['Error']['Code'] == 'InternalServiceErrorException':
            raise e
        elif e.response['Error']['Code'] == 'InvalidParameterException':
            raise e
        elif e.response['Error']['Code'] == 'InvalidRequestException':
            raise e
        elif e.response['Error']['Code'] == 'ResourceNotFoundException':
            raise e
    else:
        if 'SecretString' in get_secret_value_response:
            secret = json.loads(get_secret_value_response['SecretString'])
            # Values Substitution
            query_lead =""" select * from "DATAFLO_DATABASE"."GA_ADS_JAYANLIVE_01042021"."ACCOUNT" """
            url=URL(user=secret['SF_Username'],password=secret['SF_Password'],database=secret['SF_Database'],
            schema=secret['SF_Schema'],account=secret['SF_Account'],role=secret['SF_Role'],warehouse=secret['SF_Warehouse'])
            engine = create_engine(url)
            connection = engine.connect()
            raw_df = pd.read_sql(query_lead, connection)
            
            #Filter the needed columns
            df_session=raw_df[['date','session_duration_bucket','hits','sessions','sessions_per_user',
                               'avg_session_duration', 'bounces','session_duration','bounce_rate']]
            df_session['date']=df_session['date'].astype('datetime64[ns]')
            df_session=df_session[(df_session['date'] >='2021-01-01') & (df_session['date'] <='2021-04-30')]
            df_session=df_session.groupby('date').agg({'sessions': 'sum', 'sessions_per_user': 'sum',
                                               'session_duration':'sum','bounces':'sum'}).reset_index()
            df=df_session.groupby(pd.Grouper(key="date", freq="D")).sum().reset_index()
            df['bounce_rate(%)']=(df['bounces']/df['sessions'])*100
            df = df.astype({"sessions_per_user":'int', "session_duration":'int',"bounce_rate(%)":'int'})
            df.date = pd.to_datetime(df.date, format='%Y%m%d')
            df = df.sort_values(by="date")
            df = df.reset_index(drop=True)
            return df
        

def read_local_data():
    df_raw=pd.read_csv('GA_session_raw')
    df_session=df_raw[['date','session_duration_bucket','hits','sessions','sessions_per_user',
                               'avg_session_duration', 'bounces','session_duration','bounce_rate']]
    df_session['date']=df_session['date'].astype('datetime64[ns]')
    df_session=df_session[(df_session['date'] >='2021-01-01') & (df_session['date'] <='2021-03-31')]
    df_session=df_session.groupby('date').agg({'sessions': 'sum', 'sessions_per_user': 'sum',
                                               'session_duration':'sum','bounces':'sum'}).reset_index()
    df=df_session.groupby(pd.Grouper(key="date", freq="D")).sum().reset_index()
    df['bounce_rate(%)']=(df['bounces']/df['sessions'])*100
    df = df.astype({"sessions_per_user":'int', "session_duration":'int',"bounce_rate(%)":'int'})
    df.date = pd.to_datetime(df.date, format='%Y%m%d')
    df = df.sort_values(by="date")
    df = df.reset_index(drop=True)
    df_process=process_data(df)
    df_detect=detect_classify_anomalies(df_process,7)
    return df_detect

def process_data(df):
    actual_vals = df.sessions.values
    actual_log = np.log10(actual_vals)
    train, test = actual_vals[0:-30], actual_vals[-30:]
    train_log, test_log = np.log10(train), np.log10(test)
    my_order = (1, 1, 1)
    my_seasonal_order = (0, 1, 1, 7)
    history = [x for x in train_log]
    predictions = list()
    predict_log=list()
    for t in range(len(test_log)):
        model = sm.tsa.SARIMAX(history, order=my_order,seasonal_order=my_seasonal_order, enforce_stationarity=False,
                               enforce_invertibility=False)
        model_fit = model.fit(disp=0)
        output = model_fit.forecast()
        predict_log.append(output[0])
        yhat = 10**output[0]
        predictions.append(yhat)
        obs = test_log[t]
        history.append(obs)
    predicted_df=pd.DataFrame()
    predicted_df['load_date']=df['date'][-30:]
    predicted_df['actuals']=test
    predicted_df['predicted']=predictions
    predicted_df.reset_index(inplace=True)
    del predicted_df['index']
    return predicted_df
    
    
def detect_classify_anomalies(df,window):
    df.replace([np.inf,-np.inf], np.NaN, inplace=True)
    df.fillna(0,inplace=True)
    df['error']=df['actuals']-df['predicted']
    df['percentage_change'] = ((df['actuals'] - df['predicted']) / df['actuals']) * 100
    df['meanval'] = df['error'].rolling(window=window).mean()
    df['deviation'] = df['error'].rolling(window=window).std()
    df['-3s'] = df['meanval'] - (2 * df['deviation'])
    df['3s'] = df['meanval'] + (2 * df['deviation'])
    df['-2s'] = df['meanval'] - (1.75 * df['deviation'])
    df['2s'] = df['meanval'] + (1.75 * df['deviation'])
    df['-1s'] = df['meanval'] - (1.5 * df['deviation'])
    df['1s'] = df['meanval'] + (1.5 * df['deviation'])
    cut_list = df[['error', '-3s', '-2s', '-1s', 'meanval', '1s', '2s', '3s']]
    cut_values = cut_list.values
    cut_sort = np.sort(cut_values)
    df['impact'] = [(lambda x: np.where(cut_sort == df['error'][x])[1][0])(x) for x in
                               range(len(df['error']))]
    severity = {0: 3, 1: 2, 2: 1, 3: 0, 4: 0, 5: 1, 6: 2, 7: 3}
    region = {0: "NEGATIVE", 1: "NEGATIVE", 2: "NEGATIVE", 3: "NEGATIVE", 4: "POSITIVE", 5: "POSITIVE", 6: "POSITIVE",
              7: "POSITIVE"}
    df['color'] =  df['impact'].map(severity)
    df['region'] = df['impact'].map(region)
    df['anomaly_points'] = np.where(df['color'] == 3, df['error'], np.nan)
    df = df.sort_values(by='load_date', ascending=False)
    df.load_date = pd.to_datetime(df['load_date'].astype(str), format="%Y-%m-%d")
    df_json=df.to_json(orient='index')
    return df_json 
    
def forecast_result():
    js=read_local_data()
    return js
    
forecast_result()